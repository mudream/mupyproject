# coding=big5
import pygame
from pygame.locals import *
import sys,os
import object

R = 15
RED = (255,0,0)
mUP = 0
mDOWN = 1
mLEFT = 2
mRIGHT = 3

def Distance(p1,p2) :
	dx = p1[0] - p2[0]
	dy = p1[1] - p2[1]
	return dx*dx + dy*dy

class Editor1 :
	def __init__(self) :
		self.wsize = (600,600)
		self.screen = pygame.display.set_mode(self.wsize,0,32)
		pygame.display.set_caption("Map Editor1")
		self.bg = pygame.image.load("imgs\\bg.png")
		
		## OBJECT SETTING
		self.circles = []
		self.Objtree = object.Object(self.screen)
		self.Objtree.loadimg("objects\\tree.png")
		self.treeps = []
		self.player = object.Object(self.screen)
		self.player.loadimg("imgs\\player.png")
		self.playerpt = [1,1]

		self.kbstatus = [False,False,False,False]

		self.Org = (0,0)

		pass
	def Run(self) :
		flag = 0
		while flag == 0 :
			self.doevent()
			for event in pygame.event.get() :
				if event.type == QUIT :
					flag = 1
					break
				if event.type == MOUSEBUTTONDOWN : self.mousedown(event)
				if event.type == KEYDOWN : self.kbdown(event)
				if event.type == KEYUP : self.kbup(event)
			self.screen.fill((0,0,0))
			self.screen.blit(self.bg,(-self.Org[0],-self.Org[1]))
			for cirs in self.circles :
				pt = (cirs[0]-self.Org[0],cirs[1]-self.Org[1])
				pygame.draw.circle(self.screen,RED,pt,R,1)
			for ps in self.treeps :
				pt = (ps[0]-self.Org[0],ps[1]-self.Org[1])
				self.Objtree.draw(pt)

			self.player.draw((self.playerpt[0]-self.Org[0],self.playerpt[1]-self.Org[1]))
			pygame.display.update()

		pass
		self.WriteFile()
	def WriteFile(self) :
		wFile = open("output.txt","w+")
		wFile.write(str(len(self.circles)) + "\n")
		for ps in self.circles :
			wFile.write(str(ps[0]) + " " + str(ps[1] ) + "\n")
		wFile.write(str(len(self.treeps)) + '\n')
		for ps in self.treeps :
			wFile.write(str(ps[0]) + " " + str(ps[1] ) + "\n")

		wFile.close()
	def doevent(self) :
		npt = [self.playerpt[0], self.playerpt[1]]
		nOrg = [self.Org[0],self.Org[1]]
		if self.kbstatus[mUP] : 
			npt[1] -= 1
		if self.kbstatus[mDOWN] : 
			npt[1] += 1
		if self.kbstatus[mLEFT] : 
			npt[0] -= 1
		if self.kbstatus[mRIGHT] : 
			npt[0] += 1
		
		for ps in (self.circles) :
			if Distance(ps,npt) <= R*R :
				return 0
		for ps in self.treeps :
			if Distance(ps,npt) <= self.Objtree.radius**2 :
				return 0
		self.playerpt = [npt[0], npt[1]]

		if self.kbstatus[mUP] :
			if npt[1] - nOrg[1] < 100 and nOrg[1] > 0:
				nOrg[1] -= 1
		if self.kbstatus[mDOWN] :
			if npt[1] - nOrg[1] > 500 and nOrg[1] < 300:
				nOrg[1] += 1
		if self.kbstatus[mLEFT] :
			if npt[0] - nOrg[0] < 100 and nOrg[0] > 0:
				nOrg[0] -= 1
		if self.kbstatus[mRIGHT] :
			if npt[0] - nOrg[0] > 500 and nOrg[0] < 300:
				nOrg[0] += 1
		self.Org = [nOrg[0],nOrg[1]]
	def kbdown(self,event) :
		key = event.key
		if key == K_UP : self.kbstatus[mUP] = True
		if key == K_DOWN : self.kbstatus[mDOWN] = True
		if key == K_LEFT : self.kbstatus[mLEFT] = True
		if key == K_RIGHT : self.kbstatus[mRIGHT] = True
	def kbup(self,event) :
		key = event.key
		if key == K_UP : self.kbstatus[mUP] = False
		if key == K_DOWN : self.kbstatus[mDOWN] = False
		if key == K_LEFT : self.kbstatus[mLEFT] = False
		if key == K_RIGHT : self.kbstatus[mRIGHT] = False
	def mousedown(self,event) :
		tmppoint = pygame.mouse.get_pos()
		point = (tmppoint[0]+self.Org[0],tmppoint[1]+self.Org[1])
		button = event.button
		if button == 1 :
			flag = 0
			for ps in self.circles  :
				if Distance(ps,point) <= R*R :
					flag = 1
					self.circles.remove(ps)
					break
			if flag == 0 :
				self.circles += [point]
		if button == 3 :
			flag = 0
			for ps in self.treeps  :
				if Distance(ps,point) <= self.Objtree.radius**2 :
					flag = 1
					self.treeps.remove(ps)
					break
			if flag == 0 :
				lx = 0
				for ps in self.treeps :
					if point[1] <= ps[1] :
						break
					lx += 1
				self.treeps.insert(lx,point)
		pass

if __name__ == "__main__" :
	pygame.init()
	main = Editor1()
	main.Run()
