import pygame
from pygame.locals import *
import sys,os

class Object :
	def __init__(self,screen):
		self.Gpoint = (0,0)
		self.img = None
		self.solid = True
		self.radius = 1
		self.screen = screen
	def loadimg(self,src) :
		self.img = pygame.image.load(src)
		self.Gpoint = (self.img.get_width()/2,self.img.get_height())
		self.radius = self.img.get_width()/2
	def	draw(self,point) :
		npt = (point[0]-self.Gpoint[0],point[1]-self.Gpoint[1])
		self.screen.blit(self.img,npt)	

