# coding=big5
import pygame
from pygame.locals import *
import sys,os

class button :
	def __init__(self,screen) :
		self.text = ""
		self._press = False
		self.screen = screen
		self.pos = [0,0]
		self.rect = [0,0]
		self.textimg = None
	def unpress(self) :
		self._press = False
	def press(self) :
		self._press = True
	def settext(self,txt):
		self.text = txt
		mfont = pygame.font.Font(os.environ['SYSTEMROOT']+"\\Fonts\\msjhbd.ttf",40)
		self.textimg = mfont.render(self.text,True,(255,255,255))
		self.rect = [self.textimg.get_width(),self.textimg.get_height()]
	def inside(self,atpos) :
		if self.pos[0] < atpos[0] and self.pos[0] + self.rect[0] > atpos[0] and self.pos[1] < atpos[1] and self.pos[1] + self.rect[1] > atpos[1] :
			return True 
		else :
			return False
	def draw(self) :
		if self._press :
			bg = pygame.image.load("sel.png")
			self.screen.blit(bg,self.pos)
			self.screen.blit(self.textimg,self.pos)
		else :
			self.screen.blit(self.textimg,self.pos)
