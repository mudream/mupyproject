# coding=big5
import pygame
from pygame.locals import *
import sys,os
RED = (255,0,0)
WHITE = (255,255,255)
class List :
	def __init__(self,screen) :
		self.pos = (0,0)
		self.items = []
		self.selindex = -1
		self.screen = screen
		self.mfont = pygame.font.Font(os.environ['SYSTEMROOT']+"\\Fonts\\msjhbd.ttf",20)
		self.bg = pygame.image.load("lstbg.png")
	def selectedindex(self) :
		return self.selindex
	def mousedown(self) :
		realpos = pygame.mouse.get_pos()
		inpos = (realpos[0]-self.pos[0] , realpos[1]-self.pos[1])
#print(self.items)
		if inpos[1] > 300 or inpos[1] < 10 or inpos[0] < 0 or inpos[0] > 170 : return 0
#print(int((inpos[1]-10)/28))
		if int(int(inpos[1]-10)/28) <= len(self.items)-1 :
			self.selindex = int((inpos[1] - 10)/28)
		else :
			self.selindex = -1
#print(self.selindex)	
	def draw(self) :
		# Draw at self.pos
		self.screen.blit(self.bg,self.pos)
		for lx in range(len(self.items)) :
			dtext = None
			pt = (self.pos[0]+10,self.pos[1]+28*lx + 10)
			if lx == self.selindex :

				dtext = self.mfont.render(self.items[lx],True,RED)
				simg = pygame.image.load("lstsel.png")
				self.screen.blit(simg,pt)
			else :
				dtext = self.mfont.render(self.items[lx],True,(0,0,0))
			self.screen.blit(dtext,pt)
		#	print(dtext.get_height())
		pass 
