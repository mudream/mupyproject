# coding=big5
import pygame
from pygame.locals import *
import sys,os
import tab_status
import tab_backpack
import button
class MainList:
	def __init__(self):
		self.screen = pygame.display.set_mode((600,500),0,32)
		pygame.display.set_caption("List test")
		self.btn1 = button.button(self.screen)
		self.btn1.settext("���A")
		self.btn1.pos = [10,10]
		self.btn2 = button.button(self.screen)
		self.btn2.settext("���~")
		self.btn2.pos = [10,60]
		self.btn3 = button.button(self.screen)
		self.btn3.settext("����")
		self.btn3.pos = [10,200]
		self.btn1.press()
		
		self.StatusTab = tab_status.tab_status(self.screen)
		self.StatusTab.loadimg("player.png")
		self.BackpackTab = tab_backpack.tab_backpack(self.screen)
		self.BackpackTab.setpos([70,10])
		self.Tab = 1
	def run(self) :
		bg = pygame.image.load("bg2.png")
		while True :
			if self.Tab == 3 : break 
			for event in pygame.event.get() :
				if event.type == QUIT : exit()
				if event.type == KEYDOWN : self.kbhit()
				if event.type == MOUSEBUTTONDOWN : self.mousedown()
				if event.type == MOUSEBUTTONUP : self.mouseup()
			self.screen.fill((0,0,0))
			self.screen.blit(bg,(0,0))
			self.btn1.draw()
			self.btn2.draw()
			self.btn3.draw()
			if self.Tab == 1 :
				self.StatusTab.draw((70,10))
			elif self.Tab == 2 :
				self.BackpackTab.draw()
			elif self.Tab == 3 :
				pass
			pygame.display.update()	
		pass
	def kbhit(self) :
		key = pygame.key.get_pressed()
		pass
	def mousedown(self) :	
		pos = pygame.mouse.get_pos()
		if self.btn1.inside(pos) :
			self.btn1.press()
			self.btn2.unpress()
			self.btn3.unpress()
			self.Tab = 1
		if self.btn2.inside(pos) :
			self.btn1.unpress()
			self.btn2.press()
			self.btn3.unpress()
			self.Tab = 2
		if self.btn3.inside(pos) :
			self.btn1.unpress()
			self.btn2.unpress()
			self.btn3.press()
			self.Tab = 3
		if self.Tab == 2 :
			self.BackpackTab.mousedown()
	def mouseup(self) :
	 	pos = pygame.mouse.get_pos()
	 	pass

if __name__ == "__main__" :
	pygame.init()
	MM = MainList()
	MM.run()

