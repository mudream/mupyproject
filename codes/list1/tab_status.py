# coding=big5
import pygame
from pygame.locals import *
import sys,os

class tab_status :
	def __init__(self,screen) :
		self.HP = (75,150)
		self.MP = (75,150)
		mfont = pygame.font.Font(os.environ['SYSTEMROOT']+"\\Fonts\\msjhbd.ttf",30)
		self.HPtxt = mfont.render("HP",True,(255,255,255)) 
		self.MPtxt = mfont.render("MP",True,(255,255,255))
		self.msgtxt = mfont.render("�ڦn�i�R>//<",True,(255,255,255))
		self.img = None
		self.screen = screen
	def loadimg(self,src) :
		self.img = pygame.image.load(src)
		width = self.img.get_width()
		height = self.img.get_height()
		self.img = pygame.transform.scale(self.img,(200,int(height*200/width)))
	def draw(self,pos) :
		imgpos = (pos[0]+20,pos[1]+50)
		self.screen.blit(self.img,imgpos)
		# draw HP bar
		self.screen.blit(self.HPtxt,(pos[0]+200,pos[1]+50))	
		redp = (200,32,32)
		pygame.draw.rect(self.screen,redp,(pos[0]+200+self.MPtxt.get_width()+10,pos[1]+60,self.HP[1],25),2)
		pygame.draw.rect(self.screen,redp,(pos[0]+200+self.MPtxt.get_width()+10,pos[1]+60,self.HP[0],25),0)
		# draw MP bar
		self.screen.blit(self.MPtxt,(pos[0]+200,pos[1]+100))
		bluep = (32,32,200)
		pygame.draw.rect(self.screen,bluep,(pos[0]+200+self.MPtxt.get_width()+10,pos[1]+110,self.MP[1],25),2)
		pygame.draw.rect(self.screen,bluep,(pos[0]+200+self.MPtxt.get_width()+10,pos[1]+110,self.MP[0],25),0)
		# draw MSG 
		self.screen.blit(self.msgtxt,(pos[0]+200,pos[1]+150))

