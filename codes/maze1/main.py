# coding=big5
import pygame
import os,sys
from pygame.locals import *
import mmaze

Maze = mmaze.mmaze()
title = "maze test"
size = (400,400)
def kbhit() :
	key = pygame.key.get_pressed()
	if key[K_UP] :
		Maze.move(0,-1)
	if key[K_DOWN] :
		Maze.move(0,1)
	if key[K_LEFT] :
		Maze.move(-1,0)
	if key[K_RIGHT] :
		Maze.move(1,0)
def run():
	screen = pygame.display.set_mode(size,0,32)
	pygame.display.set_caption(title)
	Maze.setwallpicpath("block.png")
	Maze.setplayerpicpath("player.png")
	while True :
		for event in pygame.event.get() :
			if event.type == QUIT : exit()
			if event.type == KEYDOWN : kbhit()
		screen.fill((0,0,0))
		Maze.draw(screen)
		
		pygame.display.update()
	pass

if __name__ == "__main__" :
	pygame.init()

	run()
