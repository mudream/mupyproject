import pygame
class mmaze :
	def __init__(self) :
		self.table = [[1,1,1,1,1,1],
                      [1,0,0,0,0,1],	
					  [1,1,0,1,0,1],
					  [1,0,0,0,1,1],
					  [1,0,1,0,0,1],
					  [1,1,1,1,1,1]]
		self.playerpoint = [1,1]
		self.wallpic = ""
		self.playerpic = ""
	def move(self,dx,dy) :
		x = self.playerpoint[0] + dx
		y = self.playerpoint[1] + dy
		if x > 4 or x < 0 or y > 4 or y < 0 : return 0
		if self.table[x][y] == 0 :
			self.playerpoint = [x,y]
			return 1
	def setwallpicpath(self,_mpath) :
	  	self.wallpic = _mpath
	def setplayerpicpath(self,_mpath) :
	  	self.playerpic = _mpath
	def draw(self,screen) :
		player = pygame.image.load(self.playerpic)
		wall = pygame.image.load(self.wallpic)
		for lx in range(6) :
			for ly in range(6) :
				if (self.table[lx][ly] == 1) :
					screen.blit(wall,(lx*32,ly*32))
		screen.blit(player,(self.playerpoint[0]*32,self.playerpoint[1]*32))
		

	 	
