import pygame
import sys,os

import map
import obj

from pygame.locals import *

size = (800,600)


def run() :
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption("Maze~")
	Map = map.map(screen)
	Map.loadmap("map1.txt")
	Map.loadimg("img1.txt")
	playerpoint = [32,32]
	playerimg = pygame.image.load("player.png")
	pygame.display.set_icon(playerimg) # setup the icon
	moving = False
	movvoc = [0,0]
	movtar = [0,0]
	while True :
		pygame.time.delay(30)
		for event in pygame.event.get() :
			if event.type == QUIT : exit()
			if event.type == KEYDOWN and moving == False : 
				key = pygame.key.get_pressed()
				movtar = [playerpoint[0],playerpoint[1]]
				if key[K_UP] : 
					movtar[1] -= 32
					movvoc = (0,-4)
				elif key[K_DOWN] : 
					movtar[1] += 32
					movvoc = (0, 4)
				elif key[K_LEFT] : 
					movtar[0] -= 32
					movvoc = (-4,0)
				elif key[K_RIGHT] : 
					movtar[0] += 32
					movvoc = ( 4,0)
				if Map.issolid(movtar) == False :
					moving = True
				#print(movvoc,moving)
		pass
		if moving :
			if playerpoint == movtar :
				moving = False
			else :
				playerpoint[0] += movvoc[0]
				playerpoint[1] += movvoc[1]
		screen.fill((0,0,0))
		Map.draw()
		screen.blit(playerimg,playerpoint)
		pygame.display.update()
	pass

if __name__ == "__main__" :
	pygame.init()
	run()
