import pygame
from pygame.locals import *
import obj

class map :
	def __init__(self,screen) :
		self.objlist = []
		self.drawlist = []
		self.screen = screen
	def loadmap(self,src) :
		rFILE = open(src,'r')
		while True :
			line = rFILE.readline()[:-1]
			if line == '' : break
			getnums = line.split(' ')
			i1 = int(getnums[0])
			i2 = int(getnums[1])
			i3 = int(getnums[2])
			print((i1,i2,i3))
			self.drawlist.append((i1,i2,i3))
		rFILE.close()
	def loadimg(self,src) :
		rFILE = open(src,'r')
		while True :
			fn = rFILE.readline()
			if fn == '' : break
			if fn[-1] == '\n' : fn = fn[:-1]
			newobj = obj.obj(self.screen)
			newobj.setimgsrc(fn)
			self.objlist.append(newobj)	
		rFILE.close()
	def issolid(self,point):
		x,y = point[0],point[1]
		for lx in range(len(self.drawlist)) :
			index = self.drawlist[lx][0]
			nx,ny = self.drawlist[lx][1],self.drawlist[lx][2]
			if self.objlist[index].solid :
				if nx == x and ny == y :
					return True
		return False
	def draw(self) :
		for lx in range(len(self.drawlist)) :
			index = self.drawlist[lx][0]
			point = (self.drawlist[lx][1],
					self.drawlist[lx][2])
			self.screen.blit(self.objlist[index].img,point)
