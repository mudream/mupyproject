# coding=big5
import pygame
from pygame.locals import *

class obj :
	def __init__(self,screen):
		self.imgscr = ""
		self.img = None
		self.screen = screen
		self.solid = True
	def setimgsrc(self,src) :
		self.imgsrc = src
		self.img = pygame.image.load(src)
	def draw(self,point) :
		self.screen.blit(self.img,point)
