import pygame
from pygame.locals import *
import random
PSS = pygame.sprite.Sprite

White = (255,255,255)
Black = (  0,  0,  0)

height, width =  600,600

def Normalize(point):
	if point.x >= width :
		point.x -= width
	elif point.x < 0 : 
		point.x += width

	if point.y >= height :
		point.y -= height
	elif point.y < 0 : 
		point.y += height

class Player(PSS):
	def __init__(self):
		PSS.__init__(self)
		self.imgup = pygame.image.load("na.png")
		self.imglt = pygame.transform.rotate(self.imgup,90)
		self.imgdn = pygame.transform.rotate(self.imglt,90)
		self.imgrt = pygame.transform.rotate(self.imgdn,90)
		self.image = self.imgup
		self.rect = self.image.get_rect()
		self.rect.x , self.rect.y = width/2 , height/2
		self.Status = "UP"
	def update(self):
		if self.Status == "UP":
			self.image = self.imgup
		elif self.Status == "DOWN":
			self.image = self.imgdn
		elif self.Status == "LEFT":
			self.image = self.imglt
		elif self.Status == "RIGHT":
			self.image = self.imgrt
	def draw(self,screen):
		screen.blit(self.image,(self.rect.x,self.rect.y))
		
class Point(PSS):
	def __init__(self):
		PSS.__init__(self)
		self.image = pygame.Surface([20,20]).convert()
		self.image.fill(Black)
		self.image.set_colorkey(White)
		self.rect = self.image.get_rect()
		self.rect.x = random.randrange(width)
		self.rect.y = random.randrange(height)	
	def update(self):
		self.rect.x += random.randint(-3,3)
		self.rect.y += random.randint(-3,3)
		Normalize(self.rect)
		pass	

class App:
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode((height,width),0,32)
		pygame.display.set_caption("NaNa Game")

		# There is the information of kbhit
		self.KeyStatus = {}
		self.KeyStatus["UP"] = False
		self.KeyStatus["DOWN"] = False
		self.KeyStatus["LEFT"] = False
		self.KeyStatus["RIGHT"] = False
		
		self.mmPlayer = Player()
		
	def main(self):
		score = 0
		# Initialize the sprites
		Points = pygame.sprite.Group()
		for ii in range(50):
			Points.add(Point())
	
		while (True) :
			for event in pygame.event.get() :
				if event.type == QUIT : exit()
				if event.type == KEYDOWN : self.keydown(event)
				if event.type == KEYUP : self.keyup(event)
			
			self.screen.fill(White)

			# deal with collision
			ptcol = pygame.sprite.spritecollideany(self.mmPlayer,Points)
			if (ptcol != None) :
				score += 10
				Points.remove(ptcol)

			Points.update()
			Points.draw(self.screen)
			self.playermove()
			self.mmPlayer.update()
			self.mmPlayer.draw(self.screen)

			pygame.display.update()
			
	def playermove(self) :
		if self.KeyStatus["UP"] : 
			self.mmPlayer.rect.y -= 2
			self.mmPlayer.Status = "UP"
		if self.KeyStatus["DOWN"] : 
			self.mmPlayer.rect.y += 2
			self.mmPlayer.Status = "DOWN"
		if self.KeyStatus["LEFT"] : 
			self.mmPlayer.rect.x -= 2
			self.mmPlayer.Status = "LEFT"
		if self.KeyStatus["RIGHT"] : 
			self.mmPlayer.rect.x += 2
			self.mmPlayer.Status = "RIGHT"
		Normalize(self.mmPlayer.rect)

	def keyup(self,event):
		if event.key == K_UP : self.KeyStatus["UP"] = False
		if event.key == K_DOWN : self.KeyStatus["DOWN"] = False
		if event.key == K_LEFT : self.KeyStatus["LEFT"] = False
		if event.key == K_RIGHT : self.KeyStatus["RIGHT"] = False
	def keydown(self,event):
		if event.key == K_UP : self.KeyStatus["UP"] = True
		if event.key == K_DOWN : self.KeyStatus["DOWN"] = True
		if event.key == K_LEFT : self.KeyStatus["LEFT"] = True
		if event.key == K_RIGHT : self.KeyStatus["RIGHT"] = True

if __name__ == "__main__" :
	mm = App()
	mm.main()

