# coding=big5
import pygame
from pygame.locals import *
import os,sys

import oxgame

size = (350,350)
black = (0,0,0)
white = (255,255,255)
title = "OXGame"
txtwin = ["O贏了!" , "X贏了!" , "和局"]
mgame = oxgame.Oxgame()

def mouse_down(pos):
	if who == 2 : return 0
	global who
	w = 70
	for i in range(3) :
		for j in range(3) :
			if (((i+1)*w < pos[0]) and (pos[0] < (i+2)*w)) and ((j+1)*w < pos[1]) and (pos[1] < (j+2)*w) :
				if mgame.put(j+i*3,1) == 1 :
					who = 2
					return 1
def compu_turn():
	global who
	empty = 0
	for i in range(9) :
		if mgame.tab[i] == 0 :
			empty = 1
			break
	if empty == 1 :
		getpos = mgame.think(2)
		mgame.put(getpos[1],who)
		who = 1
	
def run():
	global who
	pygame.init()
	screen = pygame.display.set_mode(size,0,32)
	pygame.display.set_caption(title)
	
	mfont = pygame.font.Font(os.environ['SYSTEMROOT']+"\\Fonts\\kaiu.ttf",60)
	while True:
		if who == 2 : compu_turn()
		for event in pygame.event.get() :
			if(event.type == QUIT) :
				exit()
			elif(event.type == MOUSEBUTTONDOWN) :
			 	mouse_down(pygame.mouse.get_pos())
		screen.fill(black)
		flag = mgame.check()
		if flag > 0 :
			txtmsg = mfont.render(txtwin[flag-1],True,white)
			screen.blit(txtmsg,(100,100))	
		elif flag == -1 :
			txtmsg = mfont.render(txtwin[2],True,white)
			screen.blit(txtmsg,(100,100))
		else :
			mgame.show(screen)
		pygame.display.update()


if __name__ == "__main__" :
	# 這裡會放主程式
	global who 
	who = 1
	run()
