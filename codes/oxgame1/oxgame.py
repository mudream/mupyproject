import pygame
from pygame.locals import *

import os,sys

white = (255,255,255)

class Oxgame : 
	def __init__(self):
		self.tab = [0,0,0,0,0,0,0,0,0]
	def put(self,pos,val):
		if self.check() != 0 :
			return 0
		if self.tab[pos] == 0 :
			self.tab[pos] = val
			return 1
		return 0
	def __think(self,mtab,who) :
		chk = self.check()
		if (chk == -1) : return (0,0)
		if (chk > 0) : return (chk,0)
		losecnt = 0 
		losept = (0,0)
		safecnt = 0
		safept = (0,0)
		for pos in range(9) :
			if mtab[pos] == 0 :
				mtab[pos] = who 
				res = self.__think(mtab,3-who)
				if res[0] == who :
					mtab[pos] = 0
					return (who,pos)
				elif res[0] == 3-who :
					losecnt = losecnt + 1
					losept = (3-who,pos)
				elif res[0] == 0 :
					safecnt = safecnt + 1
					safept = (0,pos)
				mtab[pos] = 0
		if safecnt > 0 :
		 	return safept
		else : 
		 	return losept
	def think(self,who) :
		pos = self.__think(self.tab,who)
		return pos
	def check(self) :
	 	return self.__check(self.tab)
	def __check(self,_tab):
		for i in range(3) :
			if self.tab[i] == 1 and self.tab[i+3] == 1 and self.tab[i+6] == 1 :
				return 1
			if self.tab[i] == 2 and self.tab[i+3] == 2 and self.tab[i+6] == 2 :
				return 2
			if self.tab[3*i] == 1 and self.tab[3*i + 1] == 1 and self.tab[3*i + 2] == 1 :
				return 1
			if self.tab[3*i] == 2 and self.tab[3*i + 1] == 2 and self.tab[3*i + 2] == 2 :
				return 2
		if self.tab[0] == 1 and self.tab[4] == 1 and self.tab[8] == 1 :
			return 1
		if self.tab[0] == 2 and self.tab[4] == 2 and self.tab[8] == 2 :
			return 2	
		if self.tab[2] == 1 and self.tab[4] == 1 and self.tab[6] == 1 :
			return 1
		if self.tab[2] == 2 and self.tab[4] == 2 and self.tab[6] == 2 :
			return 2
		cnt = 0
		for i in range(9) :
		 	if self.tab[i] > 0 : 
		 		cnt = cnt + 1
		if cnt == 9 :
			return -1
		return 0
	def show(self,screen):
		mfont = pygame.font.Font(os.environ['SYSTEMROOT']+"\\Fonts\\impact.ttf",60)
		txtO = mfont.render("O",True,white)
		txtX = mfont.render("X",True,white)
		_wid = txtO.get_width()/2
		_heg = txtX.get_height()/2
		w = 70
		pygame.draw.line(screen,white,(w,2*w),(4*w,2*w))
		pygame.draw.line(screen,white,(w,3*w),(4*w,3*w))
		pygame.draw.line(screen,white,(2*w,w),(2*w,4*w))
		pygame.draw.line(screen,white,(3*w,w),(3*w,4*w))

		for i in range(3) :
			for j in range(3) :
				if(self.tab[i*3 + j] == 1) :
					screen.blit(txtO,((i+1)*w + w/2 - _wid,(j+1)*w + w/2 - _heg))
				elif(self.tab[i*3 + j] == 2) :
					screen.blit(txtX,((i+1)*w + w/2 - _wid,(j+1)*w + w/2 - _heg))
	
