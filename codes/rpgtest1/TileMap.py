import pygame
from pygame.locals import *

class TileMap:
	def __init__(self):
		self.AlphaColor = (255,255,255,255)
		self.PieceSize = (0,0)
		self.Img = None
		self.Height = 0
		self.Width = 0
		pass
	
	def __alphacolor(self,img):
		for i in range(img.get_width()):
			for j in range(img.get_height()):
				if img.get_at((i,j)) == self.AlphaColor :
					img.set_at((i,j),(0,0,0,0))
	
	def LoadImage(self,src,sz):
		self.PieceSize = sz
		img = pygame.image.load(src)
		self.Img = img
		imgsz = img.get_size()
		if(imgsz[0]%sz[0] != 0) or (imgsz[1]%sz[1] != 0): return 0
		widthT = imgsz[0]//sz[0]
		heightT = imgsz[1]//sz[1]
		print(widthT,heightT)
		self.Height, self.Width = heightT, widthT	
		self.Result = []
		for lx in range(widthT):
			ResultLine = []
			for ly in range(heightT):
				pos1 = (lx*sz[0],ly*sz[1])
				fdResult = img.subsurface(pygame.Rect(pos1,sz))
				fdResult = fdResult.convert_alpha()
				self.__alphacolor(fdResult)
				ResultLine.append(fdResult)
			self.Result.append(ResultLine)
	def GetPiece(self,pos):
		print(pos)
		return self.Result[pos[0]][pos[1]]
	def GetSize(self):
		return (self.Width,self.Height)
