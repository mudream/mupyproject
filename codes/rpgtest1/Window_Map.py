import pygame
from pygame.locals import *
import Window
import Map
import TileMap
ww = Window.Window
mm = Map.Map
class Window_Map(ww):
	def __init__(self,rec):
		ww.__init__(self,rec)
		self.AddPoint = (0,0)
		self.tmap = None
		self.Map = mm((20,20),1)
	def SetTileMap(self,_tmap):
		self.tmap = _tmap
	def mousedown(self,pos):
		if (ww.mousedown(self,pos) == False) : return False	
		rec = self.Rect
		npos = (pos[0]-rec[0],pos[1]-rec[1])
		sel = (npos[0]//32,npos[1]//32)
		self.Map.Map[0][sel[0]][sel[1]] = self.AddPoint[:]
		print(self.AddPoint)
		return True
	def draw(self,screen):
		rec = self.Rect
		for i in range(20):
			for j in range(20):
				if self.Map.Map[0][i][j] == (-1,-1) : continue
				pos = ( i*32+rec[0] , j*32+rec[1] )
				screen.blit(self.tmap.GetPiece(self.Map.Map[0][i][j]),pos)

		pass
		
