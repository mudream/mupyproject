import pygame
from pygame.locals import *
import TileMap
import Window
ww = Window.Window
class WinSelect(ww) :
	def __init__(self,rec):
		ww.__init__(self,rec)
		self.tmap = TileMap.TileMap()
		self.Selected = [0,0]
	def mousedown(self,pos):
		if (ww.mousedown(self,pos) == False) : return False	
		rec = self.Rect
		npos = (pos[0]-rec[0],pos[1]-rec[1])
		self.Selected[0] = npos[0]//32
		self.Selected[1] = npos[1]//32
		return True
	def draw(self,screen):
		screen.blit(self.tmap.Img,(self.Rect[0],self.Rect[1]))

