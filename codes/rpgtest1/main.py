import pygame
from pygame.locals import *

import TileMap
import Window_SelectImg
import Window_Map
import Map
ws = Window_SelectImg
wm = Window_Map

class App:
	def __init__(self):
		pass
	def Init(self):
		pygame.display.set_mode((900,640),0,32)
		pygame.display.set_caption("MM RPG")
		self.screen = pygame.display.get_surface()
		self.winsel = ws.WinSelect((640+1,0,256,640))
		self.winsel.tmap.LoadImage("imgs\\tilpic_darkspace.png",(32,32))
		self.winmap = Window_Map.Window_Map((0,0,640,640))
		self.winmap.SetTileMap(self.winsel.tmap)
	def Run(self):
		while True :
			for event in pygame.event.get():
				if event.type == QUIT : exit()
				if event.type == MOUSEBUTTONDOWN :
					self.mousedown(event.pos)
			self.winsel.draw(self.screen)
			self.winmap.draw(self.screen)
			pygame.display.update()
	def mousedown(self,pos):
		self.winmap.mousedown(pos)
		if self.winsel.mousedown(pos) == True:
			self.winmap.AddPoint = self.winsel.Selected

		


def Initialize():
	pygame.init()

def main():
	MyApp = App()
	Initialize()
	MyApp.Init()
	MyApp.Run()

if __name__ == "__main__" :
	main()
