import os
import pygame

from pygame.locals import *

white = (255,255,255)
black = (0,0,0)
class window :
	def __init__(self,x,y,width,height) :
		self.pos = (x,y)
		self._width = width 
		self._height = height
		self.content = pygame.Surface((width,height))
		self.bg = pygame.image.load("img\\msgbg.png")
		tmp = pygame.transform.scale(self.bg,(width,height))
		self.bg = tmp
		self.content.blit(self.bg,(0,0))
		self.txts = []
	def drawtext(self,text,pos) :
		self.txts += [(text,pos)]
		print(self.txts)
	def drawimg(self,src,pos) :
		pass
	def draw(self,screen) :	
		screen.blit(self.bg,self.pos)
		fntpath = pygame.font.match_font('bitstreamverasans')
		mfont = pygame.font.Font(fntpath,20)
		for tt in self.txts:
			img = mfont.render(tt[0],True,black)
			screen.blit(img,(tt[1][0]+self.pos[0],tt[1][1]+self.pos[1]))
